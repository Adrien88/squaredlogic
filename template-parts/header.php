<?php

    /**
     *  Prepare Body classes
     */
    $bodyClass = '';
    $bodyClass .= get_theme_mod('MainColorThem');
    $bodyClass .= ' '.get_theme_mod('BGColor');
    $bodyClass .= ' '.get_theme_mod('FGColor');
    $bodyClass .= ' '.get_theme_mod('WidthMain');
    $bodyClass .= ' '.get_theme_mod('FontFamily');
    $bodyClass .= ' '.get_theme_mod('FontSize');

?>

<!DOCTYPE html>
<html lang="<?=get_bloginfo('language');?>">
<head>
    <meta charset="<?=get_bloginfo('charset');?>">
    <?php wp_head(); ?>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <!-- BootStrap 4.5.3 -->
    <link rel="stylesheet" href="<?=home_url()?>/wp-content/themes/TheArtist/assets/bootstrap-4.5.3-dist/css/bootstrap-grid.min.css" type="text/css">
    <link rel="stylesheet" href="<?=home_url()?>/wp-content/themes/TheArtist/assets/bootstrap-4.5.3-dist/css/bootstrap-reboot.min.css" type="text/css">
    <link rel="stylesheet" href="<?=home_url()?>/wp-content/themes/TheArtist/assets/bootstrap-4.5.3-dist/css/bootstrap.min.css" type="text/css">

    <!-- Bootstrap 5 -->
        <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous"> -->

    <!-- Theme -->
    <link rel="stylesheet" href="<?=home_url()?>/wp-content/themes/TheArtist/assets/css/main.css" type="text/css">
    <link rel="stylesheet" href="<?=home_url()?>/wp-content/themes/TheArtist/assets/css/fonts.css" type="text/css">
    <link rel="stylesheet" href="<?=home_url()?>/wp-content/themes/TheArtist/assets/css/responsive.css" type="text/css">
    <link rel="stylesheet" href="<?=home_url()?>/wp-content/themes/TheArtist/assets/css/colors.css" type="text/css">
</head>


<body id="body" class="<?=$bodyClass?>">

