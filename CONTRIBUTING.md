## Author 
- [BOILLEY Adrien (Gitlab)](https://gitlab.com/Adrien88)

## Contributors
- [BraadMartin (Github)](https://github.com/BraadMartin) with [alpha-color-picker](https://github.com/BraadMartin/components/tree/master/customizer/alpha-color-picker) under GPL.

## Librairies used
- jquery 3.5.1
- popper ? 
- bootstrap 4.5.3

## Thanks
- [Graphikart (Youtube)](https://www.youtube.com/channel/UCj_iGliGCkLcHSZ8eqVNPDQ)