class popup
{

    /**
     * 
     * @param {*} name name of popup
     * @param {*} content content of popup
     */
    constructor(name, content, append = false)
    {
        this.id = 'popup_'+name || 'popup_'+'default';
        this.title = name || 'default';

        if (content !== undefined){
            if (!this.exist()) this.new();
            this.pushContent(content, append);
        }
    }
    

    /**
     * add content (instance of Element OR string/int/double)
     * @param {*} content 
     */ 
    pushContent(content, append = false)
    {
        this.content = content;
        if (this.exist()){
            let elemnt = this.get();
            let container = elemnt.getElementsByClassName('popup-content')[0];
            if (content instanceof Element){
                container.appendChild(content);
            } else {
                if (append === true){
                    container.innerHTML += content;
                } else {
                    container.innerHTML = content;
                }
            }
        }
    }

    /**
     * now if window exist
     * @return {bool} exist 
     */
    exist()
    {
        if (document.getElementById(this.id) == null) return false;
        return true
    }

    /**
     * get popup window
     * @return {null|Element} element if found, null otherwise.
     */   
    get()
    {
        if (this.exist()) return document.getElementById(this.id);
        return null;
    }

    /**
     * write a new window
     * @return {Element}  new window
     */
    new() 
    {
        let window = document.createElement('div');
        window.id = this.id;
        window.classList.add('popup-window');
        window.classList.add('container');
        window.style.justifyContent = 'center';
        window.style.alignItems = 'center';


        let title = document.createElement('div');
        title.setAttribute('class', 'popup-title');

        let titletext = document.createElement('h4');
        titletext.innerHTML = this.title;
       

        // minifier button
        let minifierButton = document.createElement('button');
        minifierButton.innerHTML = '<span class="dashicons dashicons-minus"></span>'; 
        minifierButton.addEventListener('click',(e)=>{
            this.erase();
        });
        
        // close button
        let closeButton = document.createElement('button');
        closeButton.innerHTML = '<span class="dashicons dashicons-no"></span>'; 
        closeButton.addEventListener('click',(e)=>{
            this.destroy();
        });
        
        title.appendChild(closeButton);
        title.appendChild(minifierButton);
        title.appendChild(titletext);
    
        window.appendChild(title);
        
        let content = document.createElement('div');
        content.setAttribute('class', 'popup-content');
        window.appendChild(content);
        
        // push window element
        let popupframe = this.getPopupFrame();
        popupframe.appendChild(window);
        return window;
    }

    /**
     * get popup frame
     */
    getPopupFrame()
    {
        // get popup frame
        let popupframe = document.getElementById('popupframe');
        if (popupframe == null){
            popupframe = document.createElement('div');
            popupframe.id = 'popupframe';
            document.getElementsByTagName('body')[0].appendChild(popupframe);
        }
        popupframe.style.display = 'flex';
        return popupframe;
    }

    /**
     * remove window frame from popupframe
     */
    erase() 
    {
        if (this.exist()){
            let popupframe = this.getPopupFrame();
            let window = document.getElementById(this.id);
            popupframe.removeChild(window);
            // if empty's popup frame : hidde it. 
            if ( popupframe.childElementCount == 0)
                popupframe.style.display = 'none';
        }
    }

    /**
     * destroy 
     */
    destroy()
    {
        this.erase();
        this.id = null;
        this.title = null;
        this.content = null;
        return null;
    }


}


