// console.log('admin-themCustomize.js :: loaded');
(function($, log){

    let listContent = ['BackColor', 'FontColor',
        'PageHeaderMode', 'PageHeaderBGColor','PageHeaderBGTColor',
        'PageMainMode','PageMainBGColor','PageMainBGTColor',
        'PageFooterMode','PageFooterBGColor','PageFooterBGTColor',
    ];

    for (const varname of listContent) {
        wp.customize('ThemCustomize_'+varname, function(el){
            el.bind(function(newval){
                let root = document.documentElement;
                root.style.setProperty('--ThemCustomize_'+varname, newval+' !important;');
            });
        });
    }

    wp.customize('ThemCustomize_PageHeaderWallpaper', function(el){
        el.bind(function(newval){
            $('#header').attr('style', 'background-image : url('+newval+') !important;');
        });
    });
    wp.customize('ThemCustomize_PageMainWallpaper', function(el){
        el.bind(function(newval){
            $('#main').attr('style', 'background-image : url('+newval+') !important;');
        });
    });
    wp.customize('ThemCustomize_PageFooterWallpaper', function(el){
        el.bind(function(newval){
            console.log(newval);
            $('#footer').attr('style', 'background-image : url('+newval+') !important;');
        });
    });
    



})(jQuery, console.log)  