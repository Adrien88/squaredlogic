/**
 * AVAILABLE users Shortcuts
 * 
 */
window.addEventListener('keydown',(e)=>{
    
    // get searh function
    if (e.ctrlKey && e.shiftKey && e.key == ' ') {
        let elmnt = document.getElementById('searchForm').cloneNode(true);
        elmnt.classList.remove('hidden');
        new popup('searchForm', elmnt , false);
    }

    // new popup('searchForm', document.getElementById('searchForm').cloneNode(true), false);

    // Ctrl + Maj + L : get login page
    if (e.ctrlKey && e.shiftKey && e.key == 'L')
    window.location.href = home_url+'/wp-login.php';

    // Ctrl + Maj + D : get dashboard (if user logged)
    if (e.ctrlKey && e.shiftKey && e.key == 'D')
    window.location.href = home_url+'/wp-admin/';

    // remove 
    // if (e.key == 'Escape')
    //     listpupopframe = document.getElementById('popupframe');
    //     for(const win of listpupopframe.children){
    //         listpupopframe.removeChild(win);
    //     }
    //     listpupopframe.style.display = 'none';
});
