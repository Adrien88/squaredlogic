
/**
 * Push menu animation
 */
anime_menus();  

/** 
 * Animation function
 */
function anime_menus()
{
    // for focus menu : 
    for(let menus of document.getElementsByClassName('menu')){
    
        for(let link of menus.getElementsByTagName('a')){
            let parent = link.parentElement;
            let href = link.getAttribute('href');
            if (href != undefined && href != ''){
                link.removeAttribute('href');
                parent.addEventListener('click', (e)=>{
                    // make li clickable instead of contained link himself
                    window.location.href = href;
                });
            }
        }

        for(let obj of menus.getElementsByClassName('nav-dropdown')){
            
            let link = obj.getElementsByTagName('a')[0];
            let dropdown = obj.getElementsByTagName('ul')[0];
        
            // 
            if (dropdown !== undefined){

                // move ul.dropdown below li.nav-dropdown
                if(obj.classList.contains('after')){
                    let parent = obj.parentElement;
                    obj.removeChild(dropdown);
                    parent.appendChild(dropdown);
                }
                // initiate display none
                dropdown.style.display = dropdown.style.display;

                // 
                link.parentElement.addEventListener('click', (event)=>{
                    if(dropdown !== undefined){
                        if(dropdown.style.display == 'block'){
                            dropdown.style.display = 'none';
                            obj.classList.remove('active');
                        } 
                        else {
                            dropdown.style.display = 'block';
                            obj.classList.add('active');
                        }
                    }
                });
            }

        }
    }
}
