<?php

use them\includes\CustomPostTypes;
use them\includes\reCAPTCHA;
use them\includes\ThemInstall;
use them\includes\ThemCustomize;
use them\includes\Taxonomy;

// include config
$config = [
    'thumbs' => ['post', 'page'],
];

// get yaml
include_once __DIR__.'/includes/yaml/Includer.php';

// include gloabales functions
include_once __DIR__.'/includes/functions.php';
include_once __DIR__.'/includes/class-AdminModel.php';

// installer
include_once __DIR__.'/includes/class-ThemInstall.php';
ThemInstall::atInstall();

// 
include_once __DIR__.'/includes/class-ThemMenuWalker.php';

include_once __DIR__.'/includes/class-ThemCustomize.php';
ThemCustomize::init();

include_once __DIR__.'/includes/classManager-CustomPostTypes.php';
$handler = new CustomPostTypes;
$config['thumbs'][] = $handler->getRegistredKeys();


// $registred = ThemContentTypes::getRegistredKeys() ?? [];
// $config['thumbs'] = array_merge($config['thumbs'], $registred);

// include_once __DIR__.'/includes/classManager-Taxons.php';
// new Taxonomy;

them\thumbs($config['thumbs']);

// var_dump($thumbSupport);

// Register::themOptions();
// Register::themInterface();

/**
 * 
 */
function set_them_directory( string $path = 'wp-content/themes/squaredlogic' )
{
    global $wp;
    $wp->them_directory = $path;
    $wp->them_url = home_url().'/'.$path;
}
set_them_directory();

/**
 * 
 */
function get_them_directory()
{
    global $wp;
    return $wp->them_directory;
}

/**
 * 
 */
function get_them_url()
{
    global $wp;
    return $wp->them_url;
}



// var_dump($wp);



// actions 
add_action('after_setup_theme', function(){
    add_theme_support('title-tag');
    add_theme_support('menus');
    add_theme_support('html5');
    add_theme_support('custom-logo', [
        'height'      => 200,
        'width'       => 400,
        'flex-height' => true,
        'flex-width'  => true,
        'header-text' => array( 'site-title', 'site-description' ),
        'unlink-homepage-logo' => true, 
    ]);
    register_nav_menu('navbar', 'Menu principal');
    register_nav_menu('legals', 'Menu légal');
    register_nav_menu('networks', 'Réseaux Sociaux');
});

// filters for title
add_filter('document_title_separator', function(){return ' | ';});

// enqueue admin script
add_action('admin_enqueue_scripts', function(){
    wp_deregister_script('jquery');
    wp_enqueue_script('jquery', get_template_directory_uri().'/assets/jQuery.3.5.1.js');
    wp_enqueue_style('them_admin_style', get_template_directory_uri().'/assets/css/admin.css');

    // wp_enqueue_script('alpha-color-picker', get_template_directory_uri().'/assets/js/alpha-color-picker.js', ['jquery','wp-color-picker'],'1.0.0',true);
    // wp_enqueue_style('alpha-color-picker', get_template_directory_uri().'/assets/css/alpha-color-picker.css', ['wp-color-picker'],'1.0.0');
});

// enqueue admin script
add_action('customize_preview_init', function(){
    wp_enqueue_script('them_admin_script', get_template_directory_uri().'/assets/js/admin-themCustomize.js', ['jquery', 'customize-preview'], '', true);
});

// enqeue dashicons script
add_action('wp_enqueue_scripts', function(){
    wp_deregister_script('jquery');
    wp_enqueue_script('jquery', get_template_directory_uri().'/assets/jQuery.3.5.1.js');
    wp_enqueue_style('dashicons');
    
});

/**
 * Adding SideBarres 
 * 
 */
add_action('widgets_init', function(){
    
    include_once 'includes/widgets/Youtube.php';
    register_widget(Youtube::class);
    
    include_once 'includes/widgets/Author.php';
    register_widget(Author::class);

    include_once 'includes/widgets/SiteId.php';
    register_widget(SiteId::class);

    include_once 'includes/widgets/SiteLogo.php';
    register_widget(SiteLogo::class);


    register_sidebar([
        'id' => 'rightbar',
        'name' => 'Barre de droite',
        'description'   => 'Barre latérale de droite',
        'class'         => '',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => "</div>\n",
		'before_title'  => '<h2 class="widgettitle">',
		'after_title'   => "</h2>\n",
    ]);

    register_sidebar([
        'id' => 'leftbar',
        'name' => 'Barre de gauche',
        'description'   => 'Barre latérale de gauche',
        'class'         => '',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => "</div>\n",
		'before_title'  => '<h2 class="widgettitle">',
		'after_title'   => "</h2>\n",
    ]);

    register_sidebar([
        'id' => 'footer',
        'name' => 'Pied de page',
        'description' => 'Pied de page',
        'class'         => '',
		'before_widget' => '<div id="%1$s" class="widget flex-fill %2$s ">',
		'after_widget'  => "</div>\n",
		'before_title'  => '<h2 class="widgettitle">',
		'after_title'   => "</h2>\n",
    ]);

    register_sidebar([
        'id' => 'homepage',
        'name' => 'Page d\'accueil',
        'description' => 'Contenu de la page statique d\'accueil (si activée).',
        'class'         => '',
		'before_widget' => '<div id="%1$s" class="widget %2$s mb3">',
		'after_widget'  => "</div>\n",
		'before_title'  => '',
		'after_title'   => "\n",
    ]);

    // $the_sidebars = wp_get_sidebars_widgets();
    // var_dump($the_sidebars['footer']);
    // echo count( $the_sidebars['footer'] );


});