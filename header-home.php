<?php 
    get_template_part('header');
?>

<!-- header  -->
<header id="header" class="">
    <div class="container-fluid">
        <div class="container">
            <div class="header_text_aera">
                <h1><?=__('Articles')?></h1>
                <p><?=__('Liste des publications récentes.')?></p>
            </div>
        </div>
    </div>
</header>

<?php 
    get_template_part('navbar');
?>
