<?php 

get_header();

if (have_posts()):
    while(have_posts()): 
        the_post();

            $postthumb = get_the_post_thumbnail_url();
            $postthumb =  ($postthumb != '') ? $postthumb : get_theme_mod('defbaneer');

            // dictinct creating date from editing date
            $mod = (the_modified_date('','','',false) !== the_date('','','',false)) ? the_modified_date('','','',false) : null;
        
        ?>
            <!-- header -->
            <header id="header" class="baneer <?=get_theme_mod('defbaneermod')?>" style="background:url('<?=$postthumb?>');">
                <div class="container-fluid">
                    <div class="container">
                        <div class="header_text_aera">
                            <h1><?=the_title()?></h1>
                            <?=__('by')?>: <b><?=get_the_author();?></b> - <?=__('the')?>: <b><?=get_the_date('d/m/Y');?></b> 
                            <?php if ($mod != null){ echo ' - '.__('modified the').' <b>'.$mod.'</b>'; } ?>
                        </div>
                    </div>
                </div>
            </header>
            
            <?php 
                get_template_part('navbar');
            ?>

        <!-- main -->
        <main id="main" >
            <div class="container-fluid">
                <div class="container">
                    <div class="row">
                        <?php 
                            $mod = get_theme_mod('PrintAsides');
                            switch($mod){
                                case '' :
                                    $article = 'col-12';
                                break;
                                case 'all': 
                                    $aside = 'col-12 col-lg-3';
                                    $article = 'col-12 col-lg-6';
                                break;
                                default:
                                    $aside = 'col-12 col-lg-4';
                                    $article = 'col-12 col-lg-8';
                                break;
                            }
                            
                        if (in_array($mod,['all','leftOnly'])):  ?>   
                            <!-- left aside -->
                            <aside id="left-aside" class='<?=$aside?>'>
                                <?= get_sidebar('leftbar'); ?>
                            </aside>
                        <?php endif;  ?>

                        <!-- article -->
                        <article class="article-page <?=$article?>" >
                            <?=the_content()?>
                        </article>

                        <?php if (in_array($mod,['all','rightOnly'])):  ?>
                            <!--  right aside -->
                            <aside id="rightbar" class='<?=$aside?>'>
                                <?= get_sidebar('rightbar'); ?>
                            </aside>
                        <?php endif;  ?>

                    </div>
                    
                    <?php 
                        if (($tags = get_the_tags()) && $tags !== false){
                            $id_tags = array_map(function($tag){
                                return $tag->term_id;
                            }, $tags);
                        } else {
                            $id_tags = [];
                        }
                        $categ = get_the_category();
                        $query = new \WP_Query([
                            'post__not_in'  => [get_the_ID()],
                            'post_type'     => 'post',
                            'post_per_page' => 3,
                            'cat'           => $cat,
                            'tag__and'      => $id_tags,
                            'orderby'       => [
                                'rand'
                            ]
                        ]);

                        if ($query->have_posts()): 
                    ?>
                    <!-- Get Articles liés -->
                    <h2>Articles liés</h2>
                    <div class="card-group">
                    
                        <?php
                            while ($query->have_posts()): 
                                $query->the_post();
                                $postlinkedthumb = get_the_post_thumbnail_url();
                                $postlinkedthumb =  ($postlinkedthumb != '') ? $postlinkedthumb : get_theme_mod('defbaneer');
                        ?>
                        <!-- new post -->
     
                        <div class="card p-0 m-0" onclick='document.location.href="<?=the_permalink()?>"' >
                            <img src="<?=$postlinkedthumb?>" class="card-img" alt="...">
                            <div class="card-img-overlay m-0">
                                <h5 class="card-title"><?=the_title()?></h5>
                                <p class="card-text"><?=the_excerpt();?></p>
                            </div>
                        </div>

                        
                        <?php endwhile; wp_reset_postdata(); ?>
                    </div>
                    <?php endif;?>


                        <?php
                            if(comments_open() || get_comments_number()){
                                comments_template();
                            }
                        ?>
                    

                </div>
            </div>
        </main>
        
        <?php
    endwhile;
endif;
get_footer();
