<?php
    use \them\includes\ThemCustomize;

    
?>

<!DOCTYPE html>
<html lang="<?=get_bloginfo('language');?>">
<head>
    <meta charset="<?=get_bloginfo('charset');?>">

    <?php wp_head(); ?>
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <!-- BootStrap 5 -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">

    <!-- Them styles -->
    <link rel="stylesheet" href="<?=get_them_url()?>/assets/css/main.css" type="text/css">
    <link rel="stylesheet" href="<?=get_them_url()?>/assets/css/fonts.css" type="text/css">
    <link rel="stylesheet" href="<?=get_them_url()?>/assets/css/responsive.css" type="text/css">
    <link rel="stylesheet" href="<?=get_them_url()?>/assets/css/colors.css" type="text/css">
    

   <?php ThemCustomize::DumpCSS(); ?>

</head>


<body id="body" class="">