<?php

get_header();
?> 
    <main id="main" class="container-fluid">
        <div class="container">
            <h3>Erreur 404</h3>
            <p>
                Cette page n'existe pas. 
                <a href="<?=esc_url(home_url('/'))?>">Page d'accueil</a>
            </p>
        </div>
    </main>
<?php
get_footer();
