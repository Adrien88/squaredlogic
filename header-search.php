<?php 
    get_template_part('header');
?>

<!-- header  -->
<header id="header" class="">
    <div class="container-fluid">
        <div class="container">
            <div class="header_text_aera">
                <h1><?=__('Rechercher')?></h1>
                <p><?=__('Publications trouvées.')?></p>
            </div>
        </div>
    </div>
</header>

<?php 
    get_template_part('navbar');
?>