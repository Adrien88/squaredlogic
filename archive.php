<?php





get_header('home');

?> 

<main id="main">
    <div class="container-fluid">
        <div class="container">
            
            <?php if(have_posts()):?>    
                <div class="">
                    <div class="">

                        <?php     
                            while(have_posts()): the_post();
                            $path = null;
                            $postthumb = get_the_post_thumbnail_url((get_post()->ID), 'thumbnail');
                            $postthumb =  ($postthumb != '') ? $postthumb : get_theme_mod('defbaneer');
                        ?>

                        <div class="card bg-dark text-white" onclick='document.location.href="<?=the_permalink()?>"' >
                            <img src="<?=$postthumb?>" class="card-img" alt="...">
                            <div class="card-img-overlay">
                                <h5 class="card-title"><?=the_title()?></h5>
                                <p class="card-text"><?=the_excerpt();?></p>
                            </div>
                        </div>

                        <!-- new post -->
                         <div class="floatted htmlCard" style="background:url('<?=$postthumb?>');" onclick='document.location.href="<?=the_permalink()?>"' >
                            <div class="text_area">
                                <?=the_title()?>
                                <?= the_excerpt(); ?>   
                            </div>
                        </div> 

                        <?php endwhile; ?>
                    </div>
                </div>
                <div class="">
                    <?php them\pagination(); ?>    
                </div>

            <?php else: ?>  
                <article>
                    Ce site ne contient pas de pages.
                </article>
            <?php endif ?>

        </div>
    </div>
</main>

<?php

get_footer();
