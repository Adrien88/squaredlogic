<?php

// print navbar every where exepted in front page
// NB : front page = page d'accueil

$position = get_theme_mod('MainNavbarPosition');

if(!is_front_page() xor get_option('show_on_front') == 'posts'):
    ?>
        <nav id="navbar" class="navbar" >
            <div class="container-fluid">
                <div class="container">
                    <nav class="navbar menu navbar-expand-lg navbar-dark bg-dark">
                       
                        <!-- <a class="navbar-brand" href="<?=esc_url(home_url('/'))?>"> </a> -->
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">

                            <?php 
                                $logo = get_site_icon_url();
                                $name = get_bloginfo('name');
                                $url = home_url('/');
                            ?>

                            <a href="<?=$url?>" class="nav-link">
                                <?php if (!empty($logo)): ?>
                                    <img src="<?=$logo?>" alt="site logo" style="width:1em;height:1em">
                                <?php endif; ?>
                                <?=$name;?>
                            </a>

                            <?php 
                                wp_nav_menu([
                                    'theme_location'    =>'navbar', 
                                    'menu_class'        => 'navbar-nav me-auto',
                                    'container'         => 'ul',
                                    // 'walker'            => new \them\includes\ThemMenuWalker(),
                                ]);

                            ?>
                            <?= get_search_form(); ?>
                        </div>
                    </nav>
                </div>
            </div>
        </nav>         
    <?php 
endif;