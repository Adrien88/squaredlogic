<?php 

get_header();

if (have_posts()){
    while(have_posts()){ 
        the_post();

          
        
        ?>
            <!-- header  -->
            <header id="header" class="baneer <?=get_theme_mod('defbaneermod')?>" style="background:url('<?= $postthumb ?>');">
                <div class="container-fluid">
                    <div class="container">
                        <div class="header_text_aera">
                            <h1>
                                <?=the_title()?>
                            </h1>
                        </div>
                    </div>
                </div>
            </header>

            <?php 
                get_template_part('navbar');
            ?>

            <!-- main -->
            <main id="main">
                <div class="container-fluid">
                    <div class="container">
                        <article class="article-page">
                            <?=the_content()?>
                        </article>

                    </div>
                </div>
            </main>

        <?php
    }
} 
get_footer();

?>
