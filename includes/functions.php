<?php 
namespace them;


/**
 * Delete html tag from a string and keeping content
 * 
 * @param string $string to clean
 * @param string|array tag to delete
 * 
 * 
 */
function tag_remover($string = '', $tags = '', bool $keep = true)
{

    $replacer = function($string, $tag, $keep){
        if(in_array($tag, ['br','hr','meta','link'])){
            return preg_replace('#</?'.$tag.' ([^>][\w\s="\']*)*>#mi', '', $string);
        } else {
            $keep = ($keep === true) ? '$2' : '';
            return preg_replace('#<'.$tag.' ([^>][\w\s="\']*)*>(.*)</'.$tag.'>#mi', $keep, $string);
        }
    };

    if(is_array($tags)){
        foreach($tags as $tag){
            $string = $replacer($string, $tag, $keep);    
        }
        return $string;
    } 
    else {
        $tag = $tags;
        return $replacer($string, $tag, $keep);
    }
}


/**
 * To find, in folder, recursvly, all files with specific extansion 
 * 
 * @param string $path
 * @param string $extansion
 * @return array file list
 * 
 */
function recursive_search(string $path, string $ext = 'css')
{
    $out = [];
    if(file_exists($path)){
        if (is_dir($path)){
            foreach (glob($path.'/*') as $explore_path){
                $out = array_merge($out, recursive_search($explore_path, $ext));
            }
        } else if (is_file($path) && strpos($path, $ext, strlen($ext)*-1) !== false) {
            $out[] = substr($path, strpos($path, '/wp-content'));
        }
    }
    return $out;
}

/**
 * 
 * 
 */
// function phpFormat($array, $i = 1){
//     $t = str_repeat("\t",$i);
//     $out = "[";
//     foreach($array as $key => $value){
//         if (empty($key) or empty($value)) continue;
//         $key = str_replace(['\\', '\''], ['\\\\','\\\''], $key);
//         if (is_array($value)){
//             $out .= "\n $t'".$key."' => ".phpFormat($value, ($i+1))."";
//         } else {
//             if (is_string($value)){
//                 $value = '\''.str_replace(['\\', '\''], ['\\\\','\\\''], $value).'\'';
//             }
//             $out .= "\n $t'".$key."' => ".$value.",";
//         }
//     }
//     if ($i > 1) {
//         return $out.="\n $t],";
//     } else {
//         return $out.="\n $t]";
//     }
// }


/**
 * Register a params thumbs
 * @param array $params = ['post', 'page']
 * 
 */
function thumbs(array $array = ['post', 'page'])
{
    // add theme support
    add_theme_support( 'post-thumbnails', $array );

    // add admin thumb
    foreach($array as $admin_slug)
    {
        add_filter('manage_'.$admin_slug.'_posts_columns', function($columns){   
            return [
                "cb" => $columns["cb"],
                "thumbnails" => 'Miniatures',
                "title" => $columns["title"],
                "author" => $columns["author"],
                "comments" => $columns["comments"],
                "date" => $columns["date"],
            ];
        });
        // add thumb
        add_filter('manage_'.$admin_slug.'_posts_custom_column', function($column, $postid){   
            if ($column == 'thumbnails'){
                the_post_thumbnail('thumbnail', $postid);
            }
        }, 10, 2);
    }
}


/**
 * Automatique pagination
 * 
 */
function pagination()
{
    $pages = paginate_links(['type'=>'array']);
    if ($pages === null){return;}
    echo '<nav aria-label="Pagination" class="Pagination">';
        echo '<ul>';
        foreach($pages as $link){
            echo '<li>';
            echo $link;
            echo '</li>';
        }
        echo '</ul>';
    echo '</nav>';
}

/**
 * 
 * 
 */
function prepare(array $defaultconfig = [])
{
    if (is_single() || is_singular()) {
        
        /**
         * default baneer in singuulare
         */
        $postthumb = get_the_post_thumbnail_url((get_post()->ID), 'thumbnail');
        if ($postthumb != '')  { 
            set_theme_mod('defbaneer', $postthumb); 
        } 
        else {
            $postthumb = get_theme_mod('defbaneer');
            if ($postthumb != '')  { 
                set_theme_mod('defbaneer', $postthumb); 
            } 
            else {
                set_theme_mod('defbaneer', home_url('/').'wp-content/themes/TheArtist/assets/medias/wallpapers/Next-3840x2160.jpg');
            }
        }
    }
    
    else {
        /**
         * default baneer for "archiv" and "home"
         */
        $postthumb = get_theme_mod('defbaneer');
        if ($postthumb != '')  { 
            set_theme_mod('defbaneer', $postthumb); 
        } 
        else {
            set_theme_mod('defbaneer', home_url('/').'wp-content/themes/TheArtist/assets/medias/wallpapers/Next-3840x2160.jpg');
        }
    }
        
}
