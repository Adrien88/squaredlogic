<?php 

class Author extends WP_Widget {
    
    /**
     * @var array $defaults : Defaults options
     */
    private $defaults = [
        'authorName' => true,
        'authorAvatar' => true,
        'authorEmail' => false,
        'articleCreationDate' => true,
        'articleModifiedDate' => true,
        'articleCategorie' => false,
        'articleKeywords' => false,
    ];

    /**
     * 
     */
    public function __construct()
    {
        parent::__construct('Author','Meta article', ['description' => __('Afficher les métadonnées de la page/post courent.e, si disponnible.')]);    
    }

    /**
	 * 
	 * @param array $args     Display arguments including 'before_title', 'after_title',
	 *                        'before_widget', and 'after_widget'.
	 * @param array $instance The settings for the particular instance of the widget.
     */
    public function widget($args, $instance)
    {
        if(is_single()){
            echo $args['before_widget'];
            echo $args['before_title'].'Auteur.trice'.$args['after_title'];
        ?>
            <!-- <div class="card bg-dark"> -->
                <div class="text-center p-0 m-0">
                    <?php if($instance['authorAvatar'] == true): ?>        
                        <?=get_avatar(get_the_author_meta('ID'));?><br>
                    <?php endif; ?>
                </div>
                <div class="card-body p-0">
                    <ul class="list-unstyled m-1">

                        <?php if($instance['authorName'] == true): ?>
                            <li><b>Auteur.trice : </b><br><?=get_the_author();?></li>
                        <?php endif; ?>

                        <?php if($instance['articleCreationDate'] == true): ?>
                            <li><b>Publié le : </b><br><?=get_the_date('d/m/Y');?></li>
                        <?php endif; ?>

                        <?php if($instance['articleModifiedDate'] == true): ?>
                            <li><b>Modifié le : </b><br><?=get_the_modified_date('d/m/Y');?></li>
                        <?php endif; ?>

                        <!-- <li><b></b></li> -->
                    </ul>
                    <?php 
                        // get_the_author_meta('description');
                    ?>
                </div>
            <!-- </div> -->
            
        <?php
            echo $args['after_widget'];
        }
    }



    /**
     * 	
	 * @param array $instance Current settings.
	 * @return string Default return is 'noform'.
     */
    public function form($instance)
    {
        echo __('Rendre visible : ').'<br>'; 
        if(empty($instance)){
           $instance = $this->defaults;
        }
        foreach($instance as $key => $value){   
            $current = ($value == true) ? 'checked' : '';
            ?>
                <input class="" type="checkbox" name="<?=$this->get_field_name($key)?>" id="<?=$this->get_field_name($key)?>" <?=$current?>>
                <label for="<?=$this->get_field_name($key)?>"><?=$key?></label> <br>
            <?php
        }
    }

    /**
     * 
	 * @param array $new_instance New settings for this instance as input by the user via
	 *                            WP_Widget::form().
	 * @param array $old_instance Old settings for this instance.
	 * @return array Settings to save or bool false to cancel saving.
     */
    public function update($newInstance, $oldnstance)
    {
        $return = [];
        foreach($this->defaults as $key => $value){
            $return[$key] = (bool)$newInstance[$key] ?? $value ;
        }
        return $return;
        // return [
         
        //     '' => esc_attr($newInstance['']),
        //     '' => esc_attr($newInstance['']),
        // ];
    }


}