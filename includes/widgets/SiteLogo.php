<?php 

class SiteLogo extends WP_Widget {
    
    public function __construct()
    {
        parent::__construct('SiteLogo', __('Site logo'), ['description' => __('Afficher le logo su site.')]);    
    }

    /**
	 *
	 * @param array $args     Display arguments including 'before_title', 'after_title',
	 *                        'before_widget', and 'after_widget'.
	 * @param array $instance The settings for the particular instance of the widget.
     */
    public function widget($args, $instance)
    {
        $title = $instance['title'] ?? '';

        echo $args['before_widget'];
        
        if ($title != '')
        echo $args['before_title'].$title.$args['after_title'];

        if (is_front_page()){
            ?>
                <a href="<?=get_post_type_archive_link('post');?>"><?php the_custom_logo(); ?></a>
            <?php
        } 
        else {
            the_custom_logo();
        }


        echo $args['after_widget'];

        // $title = $args['title'] ?? '';
        // $description = $args['description'] ?? '';
        // $width = $args['width'] ?? 200;
        // $height = $args['height'] ?? 100; 
        
    }
}