<?php 

class SiteId extends WP_Widget {
    
    public function __construct()
    {
        parent::__construct('SiteId', __('Site title'), ['description' => __('Afficher le titre du site.')]);    
    }

    /**
	 *
	 * @param array $args     Display arguments including 'before_title', 'after_title',
	 *                        'before_widget', and 'after_widget'.
	 * @param array $instance The settings for the particular instance of the widget.
     */
    public function widget($args, $instance)
    {
        echo $args['before_widget'];
        echo $args['before_title'].$args['after_title'];

        $title = $args['title_container'] ?? 'h2';
        if ($title != '') echo '<'.$title.'>'.get_bloginfo('name').'</'.$title.'>';
        else echo get_bloginfo('name');
        
        $desc = $args['description_container'] ?? 'p';
        if ($desc != '') echo '<'.$desc.'>'.get_bloginfo('description').'</'.$desc.'>';
        else echo get_bloginfo('description');

        if (is_front_page()){
            ?>
                <p>
                    <a href="<?=get_post_type_archive_link('post');?>"><?=__('Visit the website')?></a>
                </p>
            <?php
        }

        echo $args['after_widget'];
    }

}