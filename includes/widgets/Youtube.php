<?php 

class Youtube extends WP_Widget {
    
    public function __construct()
    {
        parent::__construct('Youtube','Youtube', ['description' => __('Intégrer une vidéo youtube.')]);    
    }

    /**
	 *
	 * @param array $args     Display arguments including 'before_title', 'after_title',
	 *                        'before_widget', and 'after_widget'.
	 * @param array $instance The settings for the particular instance of the widget.
     */
    public function widget($args, $instance)
    {
        if (isset($instance['title']))
        {
            echo $args['before_widget'];
            echo $args['before_title'].$instance['title'].$args['after_title'];
            ?>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/<?=$instance['id']?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            <?php
            echo $args['after_widget'];
        }
    }

    /**
     * 	
	 * @param array $instance Current settings.
	 * @return string Default return is 'noform'.
     */
    public function form($instance)
    {
        $title = $instance['title'] ?? '';
        $id = $instance['id'] ?? '';
        ?>
            <label for="<?=$this->get_field_name('title')?>">Nom de la vidéo</label> <br>
            <input class="widefat" type="text" name="<?=$this->get_field_name('title')?>" id="<?=$this->get_field_name('title')?>" 
            value="<?=$title?>" placeholder="Titre vidéo"><br>

            <label for="<?=$this->get_field_name('id')?>">ID de la vidéo</label> <br>
            <input class="widefat" type="text" name="<?=$this->get_field_name('id')?>" id="<?=$this->get_field_name('id')?>" 
            value="<?=$id?>" placeholder="LKMKgihS6cw"><br>
        <?php
    }

    /**
     * 
	 * @param array $new_instance New settings for this instance as input by the user via
	 *                            WP_Widget::form().
	 * @param array $old_instance Old settings for this instance.
	 * @return array Settings to save or bool false to cancel saving.
     */
    public function update($newInstance, $oldnstance)
    {
        return [
            'title' => esc_attr($newInstance['title']),
            'id' => esc_attr($newInstance['id']),
        ];
    }



}
