<?php 

$palette = array(
    'rgba(150, 50, 220, 1)', // RGB, RGBa, and hex values supported
    '#00CC99', // Mix of color types = no problem
    'rgba(0,0,0, 1)',
    'rgba( 255, 255, 255, 1)', // Different spacing = no problem
);

return [

    // [
    //     'panel' => [
    //         'id'            => 'CustomPages',
    //         'title'         => 'Customize Pages',
    //         'description'   => 'Personaliser les pages',
    //     ],
    // ],

    /**
     * ----------------------------------------------------------------------------------------------
     * FRONT PAGE SECTION
     * 
     * 
     * 
     * ----------------------------------------------------------------------------------------------
     */ 
    [
        'section' => ['id' => 'front-page', 'title' => 'Page d\'accueil', 'description' => 'Élements de la pages d\'accueil'],
    ],
    [
        'controller' => [
            'name'      => 'widget-order',
            'html_tags' => ['#ThemFrontPage > div'  => 'flex-direction'],
            'setting'   => ['default'       => 'column', ],
            'control'   => [
                'controller'    => '\WP_Customize_Control',
                'type'          => 'select',
                'choices'       => [
                    'column'            => 'Verticale',
                    'column-reverse'    => 'Verticale inversée',
                    'row'               => 'Horizontale',
                    'row-reverse'       => 'Horizontale inversée',
                ],
                'label' =>  'Ordre des widgets',
            ],
        ],
    ],
    [
        'controller' => [
            'name'      => 'background-attachment',
            'html_tags' => ['#ThemFrontPage' => 'background-attachment'],
            'setting'   => ['default'       => 'fixed',],
            'control'=> [
                'controller'=> '\WP_Customize_Control',
                'type'      => 'select',
                'choices'       =>  [
                    'fixed'     => 'Fixée',
                    'scroll'    => 'Scrollable',
                ],
                'label' =>  'Mode d\'affichage du fond d\'écran :',
            ],
        ],
    ],
    [
        'controller' => [
            'name' => 'background-image',
            'html_tags' => ['#ThemFrontPage' => 'background-image'],
            'setting' => [
                'default'   => 'wp-content/themes/squaredlogic/assets/medias/wallpapers/Icecold-3840x2160.png',
            ],
            'control'=> [
                'controller'    => '\WP_Customize_Image_Control',
                'label'         => 'Image de fond d\'écran :',
            ],
        ],
    ],
    [
        'controller' => [
            'name' => 'background-color',
            'html_tags' => ['#ThemFrontPage' => 'background-color'],
            'setting' => [
                'default'               => '#dbdbdb',
            ],
            'control'=> [
                'controller'    => '\WP_Customize_Color_Control',
                'label'         => 'Couleur de fond d\'écran :',
                'show_opacity'  => true, 
                'palette'	    => $palette 
                ]
        ],
    ],
    [
        'controller' => [
            'name' => 'heading-background-color',
            'html_tags' => ['#ThemFrontPageHeading' => 'background-color'],
            'setting' => [
                'default'               => 'rgba(2,2,2,0.8)',
            ],
            'control'=> [
                'controller'    => '\Customize_Alpha_Color_Control',
                'label'         => 'Couleur de fond de l\'en-tête :',
                'show_opacity'  => true, 
                'palette'	    => $palette 
                ]
            ],
    ],
    [
        'controller' => [
            'name' => 'heading-color',
            'html_tags' => ['#ThemFrontPageHeading' => 'color'],
            'setting' => [
                'default'               => '#ffffff',
            ],
            'control'=> [
                'controller'    => '\Customize_Alpha_Color_Control',
                'label'         => 'Couleur du texte de l\'en-tête :',
                'show_opacity'  => true, 
                'palette'	    => $palette 
                ]
            ],
        ],
];