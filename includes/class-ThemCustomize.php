<?php 
namespace them\includes;

use ReflectionClass;
use WP_Customize_Color_Control;
use WP_Customize_Manager;

use them\includes\Yaml\Yaml;

/**
 * 
 * 
 */
class ThemCustomize {

    /**
     * 
     */
    const PREFIX = 'TC-';

    /**
     * 
     */
    static $options = [];

    /**
     * @method static init() : called in functions.php.
     * - Load all customizes options from the config files
     * - If is customize preview : fetch configs into customize API sections 
     *  
     * @param void
     * @return void
     */
    public static function init()
    {
        $WPManager = (is_customize_preview()) ? new \WP_Customize_Manager() : null; 

        // $panel = [
        //     'id' => 'default',
        //     'title' => 'Customize panel',
        //     'description' => 'Default customize panel'
        // ];

        $section = [
            'id' => 'default',
            'title' => 'Customize',
            'description' => 'Default customize section'
        ];

        // load all scripts 
        foreach (glob( __DIR__.'/configs/ThemCustomize/*.php') as $file){
            $data = include_once $file;

            // $yaml = Yaml::dump($data, Yaml::DUMP_MULTI_LINE_LITERAL_BLOCK);
            // $yaml_file = str_replace('.php', '.yaml', $file);
            // file_put_contents($yaml_file, $yaml);
            // if (!file_exists($yaml_file)) file_put_contents($yaml_file, $yaml);

            foreach ($data as $object){

                // if (isset($object['panel'])) {
                    
                //     $panel = array_merge($panel, $object['panel']);
                //     $panel['id'] = self::PREFIX.$panel['id']; 
                    
                //     if ($WPManager instanceof \WP_Customize_Manager)
                //     add_action('customize_register', function() use($WPManager, $panel) {
                //         if(!in_array($panel['id'], $WPManager->panels()))
                //         $WPManager->add_panel($panel['id'], [
                //             'title' => $panel['title'],
                //             'description' => $panel['description'],
                //         ]);
                //     });
                //     continue;
                // }
                
                if (isset($object['section'])) {
                    
                    $section = array_merge($section, $object['section']);

                    if (strpos($section['id'], self::PREFIX, 0) === false)
                    $section['id'] = self::PREFIX.$section['id'];

                    //  
                    // if (isset($panel['id'])) $section['panel'] = $panel['id'];
                    
                    if ($WPManager instanceof \WP_Customize_Manager)
                    add_action('customize_register', function() use($WPManager, $section) {                        
                        if(!in_array($section['id'], $WPManager->sections())){
                            
                            // echo '<pre>'.print_r($section, 1).'</pre>';
                            // [section] => TC-front-page
    
                            $WPManager->add_section($section['id'], [
                                'id' => $section['id'],
                                'title' => $section['title'],
                                'description' => $section['description'],
                                ]);
                        }
                    });
                }

                if (isset($object['controller'])) {

                    $controller = $object['controller'];
                    $controller['name']                 = $section['id'].'-'.$controller['name'];
                    $controller['control']['section']   = $section['id'];

                    if ($WPManager instanceof \WP_Customize_Manager) {
                        add_action('customize_register', function() use($WPManager, $controller) {
                            
                            // echo '<pre>'.print_r($controller, 1).'</pre>';
                            self::newCustomizeSetting($WPManager, $controller);

                        });
                    }

                    // Inject Defaults values on switching them
                    // if (!get_theme_mod($controller['name']))
                    // add_action('after_switch_theme', function()use($controller){
                    //     set_theme_mod($controller['name'], $controller['setting']['default']);
                    // });
                    
                    // keep it on options list
                    self::$options[] = $controller;

                }

            }
        }
    }

    /**
     * @method static newCustomizeSetting() : to register a customizer
     * More infor about requested array in config-ThemCustomize.php
     * 
     * @param array $options
     *      'section' => array : wp_params
     *      'name' => string : name of setting
     *      'target' => array : ['#myId', 'background-color']
     *      'setting' => array : wp_params
     *      'control' => array : wp_params
     * @return void
     */
    public static function newCustomizeSetting(\WP_Customize_Manager $manager, array $options)
    {
        // Inlcude the Alpha Color Picker control file.
        require_once __DIR__.'/class-alphaColorPicker.php';

        unset($options['html_tags']);
        
        $default = [
            'default'               => '',
            'capability'            => 'edit_theme_options',
            'sanitize_callback'     => null,
            'priority'              => 0,
            'transport'             => 'refresh',
        ];

        $options['setting'] = array_merge($default, $options['setting']);

        if (!in_array($options['name'], $manager->settings())) {
            $manager->add_setting($options['name'], $options['setting']);
            // echo '<pre>'.print_r($options['setting'], 1).'</pre>';
        }
        
        $options['control']['settings'] = $options['name'];
        $controller = $options['control']['controller'] ?? '\WP_Customize_Control';

        // echo '<pre>'.print_r($options, 1).'</pre>';
        // $manager->add_control();

        $controller = new ReflectionClass($controller);
        if ($controller->isInstantiable()){
            $controller = $controller->newInstanceArgs([$manager, $options['name'], $options['control']]);
            $manager->add_control($controller);
        }
    }





    /**
     * @method static dump css 
     * Apply changes from get_theme_mod() in CSS
     * 
     * @param void
     * @return void
     */
    public static function DumpCSS()
    {
        ?><style type="text/css" media="all"><?php

        foreach(self::$options as $opt) {    

            // var_dump($opt); // ThemCustomize-background-attachment ??

            if (isset($opt['name'])) {
            // ATTENTION : les variables options ne sont pas prefixées. 
            // if (isset($opt['name']) && strpos($opt['name'], self::PREFIX, 0) !== false) {

                $registredkey = $opt['name'];
                
                // var_dump($registredkey);
                
                if(($registredvalue = get_theme_mod($registredkey)) != null){
                    
                    foreach($opt['html_tags'] as $tagname => $csspropertie ){
                        
                        
                        if (strpos($csspropertie, '!') !== false) {
                            $csspropertie = substr($csspropertie, 1);
                            $imp = true;
                        } else $imp = false;
                        
                        if(in_array($csspropertie, ['background-image'])) {
                            $registredvalue = 'url('.$registredvalue.')';
                        }
                        
                        if ($imp) $registredvalue .= ' !important';
                        
                        echo "\n".$tagname.'{'.$csspropertie.':'.$registredvalue.';}';
                        
                    }
                }
            }
        }
            
        ?></style><?php
    }








    /**
     * @method static dump
     * Apply changes from get_theme_mod() wth JQuery
     * 
     * @param void
     * @return void
     */
    public static function DumpJS()
    {
        
            ?> 
<script> 
(function($, wp){ 

            <?php 
                 
                foreach(self::$options as $opt)       
                if (isset($opt['name'])){

                    $registredkey = $opt['name'];           
                    if((get_theme_mod($registredkey)) != null){
                        foreach($opt['html_tags'] as $tagname => $csspropertie ){
                            ?>

//  c["ThemCustomize-front-page-background-image"] = {"settings":{"default":"ThemCustomize-front-page-background-image"},"type":"image","priority":10,"active":true,"section":"front-page","content":"<li id=\"customize-control-ThemCustomize-front-page-background-image\" class=\"customize-control customize-control-image\"><\/li>","label":"Image de fond d'\u00e9cran :","description":"","instanceNumber":37,"mime_type":"image","button_labels":{"select":"S\u00e9lectionner une image","site_icon":"S\u00e9lectionner l\u2019ic\u00f4ne du site","change":"Changer l\u2019image","default":"Valeur par d\u00e9faut","remove":"Retirer","placeholder":"Aucune image s\u00e9lectionn\u00e9e","frame_title":"S\u00e9lectionner une image","frame_button":"Choisir une image"},"canUpload":true,"attachment":{"id":34,"title":"pirate.flag","filename":"pirate.flag_.jpg","url":"http:\/\/127.0.0.1\/wordpress\/wp-content\/uploads\/2021\/01\/pirate.flag_.jpg","link":"http:\/\/127.0.0.1\/wordpress\/pirate-flag\/","alt":"","author":"1","description":"","caption":"","name":"pirate-flag","status":"inherit","uploadedTo":0,"date":1610545181000,"modified":1610545181000,"menuOrder":0,"mime":"image\/jpeg","type":"image","subtype":"jpeg","icon":"http:\/\/127.0.0.1\/wordpress\/wp-includes\/images\/media\/default.png","dateFormatted":"13 janvier 2021","nonces":{"update":"ef3eacf2a0","delete":"3d28c948c2","edit":"d017a295f5"},"editLink":"http:\/\/127.0.0.1\/wordpress\/wp-admin\/post.php?post=34&action=edit","meta":false,"authorName":"dev","authorLink":"http:\/\/127.0.0.1\/wordpress\/wp-admin\/profile.php","filesizeInBytes":31092,"filesizeHumanReadable":"30 Ko","context":"","height":261,"width":564,"orientation":"landscape","sizes":{"thumbnail":{"height":150,"width":150,"url":"http:\/\/127.0.0.1\/wordpress\/wp-content\/uploads\/2021\/01\/pirate.flag_-150x150.jpg","orientation":"landscape"},"medium":{"height":139,"width":300,"url":"http:\/\/127.0.0.1\/wordpress\/wp-content\/uploads\/2021\/01\/pirate.flag_-300x139.jpg","orientation":"landscape"},"full":{"url":"http:\/\/127.0.0.1\/wordpress\/wp-content\/uploads\/2021\/01\/pirate.flag_.jpg","height":261,"width":564,"orientation":"landscape"}},"compat":{"item":"","meta":""}}}; 

wp.customize('<?=$registredkey?>', function(el){
    // console.log(el);
    el.bind(function(newval){
        if (newval.match(/^https?:\/\//)) newval = 'url('+newval+')';
                            
                            <?php 
                             #  if css propertie is important
                            if (strpos($csspropertie, '!') !== false) {
                                $csspropertie = substr($csspropertie, 1)
                                ?> 
                                // let  str = '<?=$csspropertie?> : '+newval+' !important';
                                // console.log(str);
                                // let style = $('<?=$tagname?>').attr('style') ?? '';
                                // str = style+str;
                                // console.log(str);
                                // $('<?=$tagname?>').attr('style', str);
                                        
                            <?php  
                            #  if not important css propertie
                            } 
                            // else {
                            ?>
                                   
        console.log('<?=$tagname?>'+' : '+'<?=$csspropertie?>'+' : '+newval+' ');
        $('<?=$tagname?>').css({'<?=$csspropertie?>' : newval});

                            <?php 
                                // } 
                            ?>
    });
});
                            <?php       
                        }

                    }

                }  // end loop 

            ?> 
                })(jQuery, wp);</script> 
            <?php

        
    }

    /**
     * end of class
     */
}