<?php 
namespace them\includes;

/**
 * 
 */
class HtmlTable
{

    /**
     * 
     */
    public function __construct(array $thead, array $tbody, array $grouped = ['delete'])
    {

    }

    /**
     * 
     */
    public function __toString()
    {
        ?>
        <table class="wp-list-table widefat fixed striped table-view-list">
        <thead>
            <tr>
                <?php 
                    foreach($this->thead as $key => $colname) { echo '<td title="'.$key.'">'.__($colname).'</td>';}                
                ?>
            </tr>
        </thead>
        <tbody>

        <?php
        foreach($this->tbody as $key => $value){
        ?>
                <tr>
                    <th>
                        <input type="checkbox" name="" id="">
                    </th>
                    <td>
                        <a href="?page=<?=$this->classname?>&edit=<?=$key?>" class="button"><?=__('Edit')?></a>
                        <a href="?page=<?=$this->classname?>&delet=<?=$key?>" class="button"><?=__('Delete')?></a>
                    </td>
                    <?php 
                        if (is_array($value)) {
                            foreach($value as $skey => $array) echo '<td title="'.$skey.'">'.$array.' ?> </td>';
        
                        }
                        else  echo '<td title="">'.$value.' ?> </td>';
                    ?>
                </tr>
                <?php
        }
        ?>
            </tbody>

            <tfoot>
                <tr>
                    <?php 
                        foreach($this->thead as $colname) { echo '<td>'.__($colname).'</td>';}                
                    ?>
                </tr>
            </tfoot>
        </table>
        <?php
    }

}