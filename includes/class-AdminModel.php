<?php
namespace them\includes;

class AdminModel
{

    /**
     * @var array $GET ($_GET data if exists)
     */
    public array $GET;

    /**
     * @var array $POST ($_POST data if exists)
     */
    public array $POST;

    /** 
     * default 
     */
    protected string $default = 'default';

    /** 
     * 
     */
    protected string $userGrant = 'edit_theme_options';

    /**
     * 
     * 
     */
    public function __construct(bool $UI = true)
    {
        $this->reflection = new \ReflectionClass(get_called_class());
        $this->classname = $this->reflection->getShortName();

        if(isset($_GET) && !empty($_GET)) $this->GET = $_GET;
        if(isset($_POST) && !empty($_POST)) $this->POST = $_POST;
        if ($UI == true) $this->useAdminInterface();
    }

    /**
     * 
     * 
     */
    public function useAdminInterface(){

        add_action('admin_menu', function() 
        {
            add_theme_page(__('Manage '.$this->classname), __($this->classname), 'edit_theme_options', $this->classname, function()
            {
                // add title page
                echo '<h2>'.get_admin_page_title().'</h2><hr>';
                if (isset($this->description))
                echo $this->description.'<hr>';

                // get functions navigation
                $methods = $this->reflection->getMethods(\ReflectionMethod::IS_STATIC); 
                echo '<nav class="tablenav top">';
                foreach($methods as $method){
                    if (strpos($method->name, '_', 0) === false){
                        $url = home_url('/').'wp-admin/themes.php?page='.$this->classname.'&func='.$method->name;
                        echo '<a class="button" href="'.$url.'">'.__($method->name).'</a> ';
                    }
                }
                echo '</nav>';
            
                // execute func
                if (isset($this->GET['func']) && !empty($this->GET['func'])){
                    $caller = $this->GET['func'];
                } 
                else if($this->reflection->getMethod($this->default)){
                    $caller = $this->default;
                }

                // add called section
                if (isset($caller)){
                    add_settings_section($caller, $caller, [get_called_class(), $caller], $this->classname);
                    # do settings
                    do_settings_sections($this->classname);
                }

             
            });
        });

    }



}