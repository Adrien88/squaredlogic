<?php 
namespace them\includes;

/**
 * 
 * 
 */
class CustomPostTypes extends AdminModel {

    /**
     * 
     */
    public function __construct()
    {
        parent::__construct();
        $this->default = 'list';
        $this->description = 'Ajout rapide de Custom Post types.';

        if (get_theme_mod(__CLASS__) !== false){
            add_action('init', function(){
                foreach(get_theme_mod(__CLASS__) as $key => $value){
                    register_post_type($key, $value);
                    add_post_type_support($key, ['title', 'editor', 'thumbnail']);
                }
            });
        }
    }


    /**
     * getRegistredKeys
     * 
     * @param void
     * @return array getRegistredKeys
     * 
     */
    public function getRegistredKeys()
    { 
        if (get_theme_mod(__CLASS__)){
            return array_keys(get_theme_mod(__CLASS__));
        }
    }

    /**
     * init 
     * 
     * @param void
     * @return void
     * 
     */
    public static function _init()
    {
        set_theme_mod(__CLASS__, null );

        if (get_theme_mod(__CLASS__) == false) 
        set_theme_mod(__CLASS__, [
            'exemple' => [
                'label'         =>  'Custom',
                'description'   =>  'Random exemple of custom post type.',
                'menu_icon'     =>  'dashicons-smiley',
                'public'        =>   true,
            ]]
        );
    }

    /**
     * function register
     * 
     * 
     */
    public function register()
    {

    }


    /**
     * post : Tretment POST VAR
     * 
     */
    public static function _post()
    {
        if(is_admin() && isset($_POST)){

            // load state
            $mod = get_theme_mod(__CLASS__);

            // action = delete
            // delete = foo
            if ($_POST['action'] == 'delete') {
                if (isset($mod[$_POST['delete']])) unset($mod[$_POST['delete']]);
                
            }

            // action = edit
            // edit = foo
            if ($_POST['action'] == 'edit') {
                $key = $_POST['edit'];
                unset($_POST['action']);
                unset($_POST['edit']);
                $mod[$key] = $_POST;
            }

            // save state
            set_theme_mod(__CLASS__, $mod);
        
        }
    }


    /**
     * 
     * 
     * 
     * 
     * 
     */
    public static function list()
    {
        ?>
        <table class="wp-list-table widefat fixed striped table-view-list">
            <thead>

                <tr>
                    <td>
                        <?=__('Action')?>
                    </td>
                    <td>
                        <?=__('Name')?>
                    </td>
                    <td>
                        <?=__('Label')?>
                    </td>
                    <td>
                        <?=__('Description')?>
                    </td>
                    <td>
                        <?=__('Icon')?>
                    </td>
                
                </tr>
            </thead>
            <tbody>

        <?php
        if (get_theme_mod(__CLASS__) !== false)
        foreach(get_theme_mod(__CLASS__) as $key => $value){
        ?>
                <tr>
                    <td>
                        <a href="?page=ContentTypes&edit=<?=$key?>" class="button"><?=__('Edit')?></a>
                        <a href="?page=ContentTypes&delet=<?=$key?>" class="button"><?=__('Delete')?></a>
                    </td>
                    <td>
                        <?=$key?>
                    </td>
                    <td>
                        <?=$value['label']?>
                    </td>
                    <td>
                        <?=$value['description']?>
                    </td>
                    <td>
                        <?=$value['menu_icon']?>
                    </td>
                </tr>
                <?php
        }
        ?>
            </tbody>
        </table>
        <?php
    }

    /**
     * 
     * 
     * 
     */
    public static function new()
    {
        if(is_admin()){
            // global $wp;
            // $form = new ThemForm('newContentType', home_url($wp->request), 'POST');
            // $form->addField('name', ['name'=>'name', 'type'=>'varchar', 'max'=>50]);
            // $form->addField('title', ['name'=>'title', 'type'=>'varchar', 'max'=>50]);
            // $form->addField('slug', ['name'=>'slug', 'type'=>'varchar', 'max'=>50]);
            // echo $form;
        }
    }
    /**
     * 
     * 
     * 
     */
    public static function edit()
    {
        if(is_admin()){

            



            // global $wp;
            // $form = new ThemForm('newContentType', home_url($wp->request), 'POST');
            // $form->addField('name', ['name'=>'name', 'type'=>'varchar', 'max'=>50]);
            // $form->addField('title', ['name'=>'title', 'type'=>'varchar', 'max'=>50]);
            // $form->addField('slug', ['name'=>'slug', 'type'=>'varchar', 'max'=>50]);
            // echo $form;
        }
    }

    public static function _delete()
    {
        if(is_admin()){
            header('Location:?');
            exit;

        }
    }

}