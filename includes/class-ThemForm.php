<?php 
namespace them\includes;

use ReflectionClass;

class ThemForm 
{

    public $id;
    public $action;
    public $method;
    protected $fieldset;

    /**
     * 
     */
    public function __construct(string $id, string $action = '', string $method='GET')
    {
        $this->id = $id;
        $this->action = $action;
        $this->method = $method;
    }


    /**
     * 
     */
    public function __toString()
    {
        ?> 
        <form action="<?=$this->action?>" method="<?=$this->method?>">
            <?php  
                settings_fields('ThemFormSet'); 
                foreach($this->fieldset as $field){
                    echo $field;
                }
                submit_button(__('submit'), 'primary', 'submit');
                submit_button(__('reset'), 'secondary small', 'reset');
            ?>
        </form>
        <?php
    }


    /**
     * 
     */
    public function addField(string $id, array $params = [], array $container = [])
    {
        $container = (empty($container)) ? ['tagname' => 'div'] : $container; 
        $params['id'] = $id;
        $params['label'] = [$id, $params['label']];
        $reflexion = new ReflectionClass('them\includes\ThemFormField');
        $this->fieldset[$id] = $reflexion->newInstanceArgs($params);
        
    }

}