<?php 
namespace them\includes;

class HtmlContainer
{

    /**
     * 
     */
    public array $properties;
    public $callable;

    /**
     * Construct a form field
     * 
     * @param array $container 
     * [
     *      'before' => '',
     *      'after' => '',
     *      'tagname' => '',
     *      'class' => [ '', '', ],
     *      'before_content' => '',
     *      'after_content' => '',
     * ]
     * @param mixed[string|callable] $content  
     * 
     */

    public function __construct(array $container = [], $content = '')
    {
        $this->content = (is_callable($content)) ? $content() : $content;
        $this->properties = $container;
    }

    /**
     * Print it 
     * 
     * @return string formated content 
     */
    public function __toString() : string
    {

        $out = '';
        if (!empty($this->container)) {

            // add before container
            if (isset($this->container['before'])) $out.= $this->container['before'];
            
            // add container
            if ($this->container['tagname'] && in_array($this->container['tagname'], ['div', 'fieldset'])){
                $out.='<'.$this->container['tagname'];
                if ($this->container['class']) $out.=implode(' ',$this->container['class']);
                $out.='>';
            }
    
            // if it's fieldset;
            if($this->container['tagname'] == 'fieldset' && !empty($this->label)){
                $out.='<legend>'.$this->label.'</legend>';
            }
            
            // before content
            if (isset($this->container['before_content'])) $out.= $this->container['before_content'];
        }

            // add content here.
            $out .= $this->content;
        

        if (!empty($this->container)){
            // add after_content
            if (isset($this->container['after_content'])) $out.= $this->container['after_content'];
    
            // close container
            if ($this->container['tagname']) $out.='</'.$this->container['tagname'].'>';
            
            // add after
            if (isset($this->container['after'])) $out.= $this->container['after'];
        }


        return $out;
    }


}