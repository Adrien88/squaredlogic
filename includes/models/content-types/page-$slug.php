<?php 

get_header();

if (have_posts()){
    while(have_posts()){ 
        the_post();

            // switch between defbaneer and thubnails
            $path = null;
            $postthumb = get_the_post_thumbnail_url();
            $postthumb =  ($postthumb != '') ? $postthumb : get_theme_mod('defbaneer');
        
        ?>
            <!-- header  -->
            <header id="header" class="baneer <?=get_theme_mod('defbaneermod')?>" style="background:url('<?= $postthumb ?>');">
                <div class="container-fluid">
                    <div class="container">
                        <div class="header_text_aera">
                            <h1>
                                <?=the_title()?>
                            </h1>
                        </div>
                    </div>
                </div>
            </header>

            <?php 
                get_template_part('navbar');
            ?>

            <!-- main -->
            <main id="main">
                <div class="container-fluid">
                    <div class="container">
                        <article class="article-page">
                            <?=the_content()?>
                        </article>

                    </div>
                </div>
            </main>

        <?php
    }
} else {


}
get_footer();

?>
