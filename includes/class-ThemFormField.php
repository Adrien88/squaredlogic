<?php 
namespace them\includes;

class ThemFormField
{

    /**
     * 
     */
    public string $tag;
    public array $properties;

    /**
     * Construct a form field
     * 
     * @param string $tag tagname
     * @param array $properties
     * @param array $label
     * @param array $container 
     * [
     *      'before' => '',
     *      'after' => '',
     *      'tagname' => '',
     *      'class' => [ '', '', ],
     *      'before_content' => '',
     *      'after_content' => '',
     * ]
     */

    public function __construct(string $tag, array $properties = [], array $label = [], array $container = [])
    {
        $this->tag = $tag;
        $this->label = $label;
        $this->container = $container;
        $this->properties = $properties;
    }

    /**
     * get formated string element
     * 
     * @return string 
     * 
     */
    public function __toString()
    {
        $container = new HtmlContainer( 
            ['tagname'=>'div', 'class'=>['fieldarea']],
            function()
            {
                $out = '';
                switch($this->tag){
                    case 'input':
                        // $elemnt = new DOMElement('input');
                        $out.= '<'.$this->tag.$this->propertiesSerializer([]). '>';
                    break; 
                    case 'textarea':
                        $value = $this->properties['value']; unset($this->properties['value']);
                        $out.= '<textarea '.$this->propertiesSerializer(['value']).'>'.$value.'</textarea>';
                    break;
                    case 'select':
                        $str = '<select';
                        $value = $this->properties['value']; unset($this->properties['value']);
                        $str .= $this->propertiesSerializer(['value']).'>';
                        if(is_array($value)){
                            foreach($value as $name => $value)
                            {
                                if (is_int($name)) $name = $value;
                                $str .= "<option value=\"$name\">$value</option>";
                            }
                        }
                        $out.= $str.'</select>';
                    break;
                    case 'fieldset':
                        $str = '<fieldset';
                        $value = $this->properties['value']; unset($this->properties['value']);
                        $str .= $this->propertiesSerializer(['value']).'>';
                        if(is_array($value)){
                            foreach($value as $name => $value)
                            {
                                if (is_int($name)) $name = $value;
                                $str .= "<input type =\"checkbox\" value=\"$name\">$value</option>";
                            }
                        }
                        $out.= $str.'</fieldset>';
                    break;
                    default:break;
                }
                return $out;
            }
        ); 
        echo $container;
  
    }




    /**
     * 
     * 
     */
    public function propertiesSerializer(array $execpt = [])
    {
        $out = '';
        foreach($this->properties as $name => $value)
        {
            if (is_int($name)) $out .= " $value";
            else if (!in_array($name, $execpt)) {
                $out .= " $name = \"$value\"";
            }
        }
        return $out;
    }


}