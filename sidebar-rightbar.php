<?php if (!dynamic_sidebar('rightbar')):?>

    <div>
        <h4 class="">Archives</h4>
        <ol class="list-unstyled mb-0">
            <?php wp_get_archives(['type'=>'monthly']); ?>
        </ol>
    </div>


<?php endif;