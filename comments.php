<h2>Commentaires</h2>
<aside class="">    

<?php 
    $allowCapcha = get_theme_mod('reCAPTCHA_allowCapcha');
    $sitekey = get_theme_mod('reCAPTCHA_sitekey');
    $userskey = get_theme_mod('reCAPTCHA_userskey');
?>

<?php 
    $nbr = absint(get_comments_number());
    comment_form(['title_reply'=>__('Laisser un commentaire'), 'submit_button'=>'<button 
    id="submit"
    class="g-recaptcha submit"
    name="submit"
    data-sitekey="'.$sitekey.'" 
    data-callback="onSubmit" 
    data-action="submit">
    Submit</button>']);
    wp_list_comments();
    paginate_comments_links();
?>
</aside>

<!-- Import reCAPTCHA  -->
<?php if(get_theme_mod('reCAPTCHA_allowCapcha') != false): ?>
    <script src="https://www.google.com/recaptcha/api.js"></script>
    <script>
    function onSubmit(token) {
        document.getElementById("commentform").submit();
    }
    </script>

<?php endif; ?>

