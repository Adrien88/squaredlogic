<!-- <div class ="d-flex"> -->

    <?php if (is_user_logged_in()): ?>
        <!-- admin : add-article -->
        <button class="btn btn-outline-secondary my-2 my-sm-0" onclick="redirect('<?=home_url()?>/wp-admin/edit.php')" title="Ajouter" > 
            <span class="dashicons dashicons-plus"></span>
        </button>
        
    <?php endif; ?>
        <!-- everybody : search -->
        <button class="btn btn-outline-secondary my-2 my-sm-0 searchFormPopup" title="Chercher">
            <span class="dashicons dashicons-search"></span>
        </button>
        
    <?php if (is_user_logged_in()): ?>
        <?php if(!is_customize_preview()): ?>
            <!-- admin : edit-layout -->
            <button class="btn btn-outline-secondary my-2 my-sm-0" onclick="redirect('<?=home_url()?>/wp-admin/customize.php?return=<?=home_url()?>/wp-admin/options-general.php')" title="Layout" >
                <span class="dashicons dashicons-layout"></span>
            </button>
        <?php endif; ?>
        <!-- admin : edit-params -->
        <button class="btn btn-outline-secondary my-2 my-sm-0" onclick="redirect('<?=home_url()?>/wp-admin/options-general.php')" title="Paramètres">
            <span class="dashicons dashicons-admin-settings"></span>
        </button>
        <!-- admin : edit-user -->
        <button class="btn btn-outline-secondary my-2 my-sm-0" onclick="redirect('<?=home_url()?>/wp-admin/profile.php')" title="Mon compte" >
            <span class="dashicons dashicons-admin-users"></span>
        </button>
        <?php else: ?>
        <!-- admin : my-account -->
        <button class="btn btn-outline-secondary my-2 my-sm-0" onclick="redirect('<?=home_url()?>/wp-login.php')" title="Mon compte" >
            <span class="dashicons dashicons-admin-users"></span>
        </button>
    <?php endif; ?>

<!-- </div> -->

<div class='hidden'>
    <form id="searchForm" action="<?= esc_url(home_url('/')) ?>" class="form-inline my-2 my-lg-0 hidden">
        <input type="text" name="s" id="" placeholder='Rechercher' value="<?php get_search_query(); ?>">
        <button class="btn btn-outline-secondary my-2 my-sm-0" title="chercher" type="submit"><span class="dashicons dashicons-search"></span></button>
        <button class="btn btn-outline-secondary my-2 my-sm-0" title="effacer" type="reset"><span class="dashicons dashicons-no-alt"></span></button>
    </form>
</div>