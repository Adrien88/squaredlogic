


<footer id="footer" >
    <div class="container-fluid">
        <div class="container">
            <div class="footer_title">
                <!-- ajouté signature propriétaire du site ici -->
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="container">
            <div class="d-flex justify-content-between mb-3">
                <?php
                    get_sidebar('footer'); 
                ?>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="container">
            <div class="footer_foot">
                Website designed by BOILLEY Adrien © 2020 (<a href="https://boilley.info">visit my website</a>) and proudly powered by Wordpress ! 
            </div>
        </div>
    </div>
</footer>

<!-- Offseting vars from PHP to JavaScript  -->
<script>

    var home_url = '<?=home_url()?>';
    var them_url = '<?=get_them_url()?>';

</script>

<!-- Import popup window -->
<!-- <script src="<?=get_them_url()?>/assets/js/popups.js"></script> -->

<!-- Import shorcuts -->
<!-- <script src="<?=get_them_url()?>/assets/js/shortcuts.js"></script> -->

<!--   -->
<script src="<?=get_them_url()?>/assets/js/main.js"></script>

<!-- Bootstrap 5  -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>


<?php 
    wp_footer();

    // if(is_customize_preview())
    // \them\includes\ThemCustomize::DumpJS();

?>

</body>
</html> 