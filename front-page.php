<?php 


if (get_option('show_on_front') != 'posts'){
    get_header();

    ?>
        <div id="ThemFrontPage">
            <div style="display:flex;" id="ThemFrontPageHeading">
                <?php get_sidebar('homepage'); ?> 
            </div>
        </div>
    <?php

    wp_footer();

    // if(is_customize_preview())
    // \them\includes\ThemCustomize::DumpJS();
}
else {
    include 'home.php';
}
